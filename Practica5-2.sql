﻿USE m1u2p5;
INSERT INTO alumno (expediente, nombre, apellidos, fecha_naz)
  VALUES ('E124', 'Pedro', 'Jimenez', CURDATE()),
         ('E444','Juan','Vazquez','2017/02/14'),
         ('E450','Manolo','Suarez','2021-03-17');
INSERT INTO modulo (codigo, nombre)
  VALUES (1, 'Bases de datos'),
         (2,'Sistemas en red'),
         (3,'Aplicaciones web');
INSERT INTO profesor (dni, nombre, direccion, tlfno)
  VALUES ('DNI31', 'Federico', 'micasa', '123123123'),
         ('DNI32','Juan','el caserio','456456456');
INSERT INTO esdelegado (`expediente-delegado`, `expediente-alumno`)
  VALUES ('E124', 'E444'),
         ('E124','E450');
INSERT INTO cursa (`expediente-alumno`, `codigo-modulo`)
  VALUES ('E124', 1),
         ('E444',1),
         ('E450',2);
INSERT INTO imparte (`dni-profesor`, `codigo-modulo`)
  VALUES ('DNI31', 1),
         ('DNI32', 2);