﻿DROP DATABASE IF EXISTS m1u2p5;
CREATE DATABASE IF NOT EXISTS m1u2p5;
USE m1u2p5;
CREATE TABLE alumno
(
  expediente varchar(100),
  nombre varchar(100),
  apellidos varchar(100),
  fecha_naz date,
  PRIMARY KEY(expediente)
);
CREATE TABLE esdelegado
(
  `expediente-delegado` varchar(100),
  `expediente-alumno` varchar(100),
  PRIMARY KEY(`expediente-delegado`,`expediente-alumno`)
);
CREATE TABLE cursa
(
  `expediente-alumno` varchar(100),
  `codigo-modulo` int,
  PRIMARY KEY (`expediente-alumno`,`codigo-modulo`)
);
CREATE TABLE modulo 
(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY(codigo)
);
CREATE TABLE imparte
(
  `dni-profesor` varchar(10),
  `codigo-modulo` int,
  PRIMARY KEY(`dni-profesor`,`codigo-modulo`),
  UNIQUE KEY (`codigo-modulo`)
);
CREATE TABLE profesor
(
  dni varchar(10),
  nombre varchar(100),
  direccion varchar(100),
  tlfno varchar(10),
  PRIMARY KEY(dni)
);
ALTER TABLE esdelegado
  ADD CONSTRAINT fkEsDelegadoAlumno1
      FOREIGN KEY (`expediente-delegado`)
      REFERENCES alumno(expediente)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  ADD CONSTRAINT fkEsDelegadoAlumno2
      FOREIGN KEY (`expediente-alumno`)
      REFERENCES alumno(expediente)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT;
ALTER TABLE cursa
  ADD CONSTRAINT fkCursaAlumno
      FOREIGN KEY (`expediente-alumno`)
      REFERENCES alumno(expediente)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  ADD CONSTRAINT fkCursaModulo
      FOREIGN KEY (`codigo-modulo`)
      REFERENCES modulo(codigo)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT;
ALTER TABLE imparte
  ADD CONSTRAINT fkImparteProfesor
      FOREIGN KEY (`dni-profesor`)
      REFERENCES profesor(dni)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  ADD CONSTRAINT fkImparteModulo
      FOREIGN KEY (`codigo-modulo`)
      REFERENCES modulo(codigo)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT;